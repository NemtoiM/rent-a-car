import template from './Notifications.template.html'
import componentStyles from './Notifications.scss'

class NotificationsController {
    constructor() {
        this.componentName = 'notificationsComponent';
    }

    $onInit() {
        console.log('hi there, I am', this.componentName);
    }
}

const bindings = {
    someInput: '<',
    someOutput: '&'
}

export const notificationsComponent = {
    controller: NotificationsController,
    controllerAs: '$ctrl',
    template,
    bindings
}