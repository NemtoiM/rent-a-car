import angular from 'angular';

// User Part
// BOOKING
import { adminBookingComponent } from './Booking/AdminBooking/AdminBooking.component';
import { adminBookingDetailsComponent } from './Booking/AdminBookingDetails/AdminBookingDetails.component';
import { adminBookingListComponent } from './Booking/AdminBookingList/AdminBookingList.component';

// Admin Part
// AUTH
import { adminLoginComponent } from './Auth/AdminLogin/AdminLogin.component';
import { adminRecoverComponent } from './Auth/AdminRecover/AdminRecover.component';
import { adminRegisterComponent } from './Auth/AdminRegister/AdminRegister.component';

//Calendar
import { calendarComponent } from './Calendar/Calendar.component';

//Cars
import { addCarComponent } from './Cars/AddCar/AddCar.component';
import { carEditComponent } from './Cars/CarEdit/CarEdit.component';
import { carsListComponent } from './Cars/CarsList/CarsList.component';

//Notifications
import { notificationsComponent } from './Notifications/Notifications.component';






export default angular.module('Admin', [])
    .component('adminLoginComponent', adminLoginComponent)
    .component('adminRecoverComponent', adminRecoverComponent)
    .component('adminRegisterComponent', adminRegisterComponent)
    .component('calendarComponent', calendarComponent)
    .component('addCarComponent', addCarComponent)
    .component('carEditComponent', carEditComponent)
    .component('carsListComponent', carsListComponent)
    .component('notificationsComponent', notificationsComponent)