import template from './AddCar.template.html'
import componentStyles from './AddCar.scss'

class AddCarController {
    constructor() {
        this.componentName = 'addCarComponent';
    }

    $onInit() {
        console.log('hi there, I am', this.componentName);
    }


}

const bindings = {
    someInput: '<',
    someOutput: '&'
}

export const addCarComponent = {
    controller: AddCarController,
    controllerAs: '$ctrl',
    template,
    bindings
}