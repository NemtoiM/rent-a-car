import template from './CarEdit.template.html'
import componentStyles from './CarEdit.scss'

class CarEditController {
    constructor() {
        this.componentName = 'carEditComponent';
    }

    $onInit() {
        console.log('hi there, I am', this.componentName);
    }
}

const bindings = {
    someInput: '<',
    someOutput: '&'
}

export const carEditComponent = {
    controller: CarEditController,
    controllerAs: '$ctrl',
    template,
    bindings
}