import template from './CarsList.template.html'
import componentStyles from './CarsList.scss'

class CarsListController {
    constructor() {
        this.componentName = 'carsListComponent';
    }

    $onInit() {
        console.log('hi there, I am', this.componentName);
    }
}

const bindings = {
    someInput: '<',
    someOutput: '&'
}

export const carsListComponent = {
    controller: CarsListController,
    controllerAs: '$ctrl',
    template,
    bindings
}