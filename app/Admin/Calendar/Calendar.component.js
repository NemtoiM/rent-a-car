import template from './Calendar.template.html'
import componentStyles from './Calendar.scss'

class CalendarController {
    constructor() {
        this.componentName = 'calendarComponent';
    }

    $onInit() {
        console.log('hi there, I am', this.componentName);
    }
}

const bindings = {
    someInput: '<',
    someOutput: '&'
}

export const calendarComponent = {
    controller: CalendarController,
    controllerAs: '$ctrl',
    template,
    bindings
}