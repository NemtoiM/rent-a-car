import angular from 'angular';
import Core from './Core/Main.module';
import Admin from './Admin/Main.module';
import Public from './Public/Main.module';



class IndexController {
    constructor() {

    }

    $onInit() {
        console.log('Index controller initalized');
    }
}

angular.module('App', ['Core', 'Admin', 'Public', 'ngRoute'])
    .controller('IndexController', IndexController)
    .config(function($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(false);
        $locationProvider.hashPrefix("");
        $routeProvider
            .when('/', {
                restrict: 'D',
                templateUrl: "Pages/Public.html"
            })
            .when('/admin', {
                restrict: 'D',
                templateUrl: "Pages/Admin.html"
            })

        .otherwise({
            template: "<h1>not</h1>"
        })
    })