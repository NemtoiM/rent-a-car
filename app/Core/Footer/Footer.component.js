import template from './Footer.template.html'
import componentStyles from './Footer.scss'

class FooterController {
    constructor() {
        this.componentName = 'footerComponent';
    }

    $onInit() {
        console.log('hi there, I am', this.componentName);
    }
}

const bindings = {
    someInput: '<',
    someOutput: '&'
}

export const footerComponent = {
    controller: FooterController,
    controllerAs: '$ctrl',
    template,
    bindings
}