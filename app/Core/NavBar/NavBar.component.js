import template from './NavBar.template.html'
import componentStyles from './NavBar.scss'

class NavBarController {
    constructor() {
        this.componentName = 'navBarComponent';
    }

    $onInit() {
        console.log('hi there, I am', this.componentName);
    }
}

const bindings = {
    someInput: '<',
    someOutput: '&'
}

export const navBarComponent = {
    controller: NavBarController,
    controllerAs: '$ctrl',
    template,
    bindings
}