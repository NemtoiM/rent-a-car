import angular from 'angular';

import { navBarComponent } from './NavBar/NavBar.component';
import { footerComponent } from './Footer/Footer.component';







export default angular.module('Core', [])
    .component('navBarComponent', navBarComponent)
    .component('footerComponent', footerComponent)