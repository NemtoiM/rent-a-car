import angular from 'angular';

// User Part
// BOOKING
import { adminBookingComponent } from './Booking/AdminBooking/AdminBooking.component';
import { adminBookingDetailsComponent } from './Booking/AdminBookingDetails/AdminBookingDetails.component';
import { adminBookingListComponent } from './Booking/AdminBookingList/AdminBookingList.component';







export default angular.module('Public', [])
    .component('adminBookingComponent', adminBookingComponent)
    .component('adminBookingDetailsComponent', adminBookingDetailsComponent)
    .component('adminBookingListComponent', adminBookingListComponent)